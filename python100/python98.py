###############################################################################################################
###################################################SREENIDHIN C C##############################################
###############################################################################################################

"""
Question:

Write a program to solve a classic ancient Chinese puzzle: 
We count 35 heads and 94 legs among the chickens and rabbits in a farm. How many rabbits and how many chickens do we have?

"""

def solver(h, l):
    rabbits = (l-(2*h))/2.0
    hens = h-rabbits
    if (rabbits).is_integer() and (hens).is_integer():
        return str(rabbits),str(hens)
    else:
        return "invalid", "invalid"
    
    
    
    
    
print "%s %s" % solver(35,94)


